﻿
anb_racial_appearance = {
	usage = game

	race_elf = {
		dna_modifiers = {
			morph = {
				mode = replace				
				gene = gene_age
				template = anb_elf_aging
				value = 1.0
			}
			accessory = {
				mode = add
				gene = beards
				template = no_beard
				range = { 0 1 } # For the randomness to work correctly
			}
		}
		weight = {
			base = 0
			modifier = {
				add = 100
				OR = {
					has_trait = race_elf
					has_trait = race_elf_dead
				}
			}
		}
	}
}