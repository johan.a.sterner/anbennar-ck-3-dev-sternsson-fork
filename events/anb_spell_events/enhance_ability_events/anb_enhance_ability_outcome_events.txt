﻿#Outcome events for the Enhance Ability spell
namespace = anb_enhance_ability_outcome

anb_enhance_ability_outcome.1 = {
	type = character_event
	title = anb_enhance_ability_outcome.1.t
	desc = anb_enhance_ability_outcome.1.d

	theme = friendly
	left_portrait = {
		character = scope:owner
		animation = idle
	}
	right_portrait = {
		trigger = { NOT = { scope:owner = scope:target } }
		character = scope:target
		animation = idle
	}

	immediate = {
		set_variable = {
			name = failure_chance
			value = 100
		}
		change_variable = {
			name = failure_chance
			subtract = scope:scheme.scheme_success_chance
		}
	}

	#Cast it!
	option = {
		name = anb_enhance_ability_outcome.1.a
		######################################################################
		#	The results roll is calculated as a double success roll - if roll fails, roll with success chance again to see if nothing happens. If roll fails twice, the spell fails
		#	Calculations:
		#	For example - say success chance = 75%
		#		Nothing happens = success chance * remaining left = 0.75 * 0.25 = 18.75% chance nothing happens
		#		Failure = 100% - 75% - 18.75% = 6.25%
		######################################################################
		random_list = {
			#Success chance
			100 = {
				modifier = {
					factor = 0
				}
				modifier = {
					add = scope:scheme.scheme_success_chance
				}
				# trigger_event = anb_enhance_ability_outcome.2
				save_scope_value_as = {
					name = spell_successful
					value = yes
				}
				custom_tooltip = anb_enhance_ability_outcome.1.a_success_tt
			}
			#Failure
			100 = {
				modifier = {
					factor = 0
				}
				modifier = {
					add = var:failure_chance
				}
				modifier = {
					factor = var:failure_chance
				}
				modifier = {
					factor = 0.01
				}
				trigger_event = anb_enhance_ability_outcome.3
				custom_tooltip = anb_enhance_ability_outcome.1.a_failure_tt
			}
			#Nothing happens
			100 = {
				modifier = {
					factor = 0
				}
				modifier = {
					add = var:failure_chance
				}
				modifier = {
					factor = scope:scheme.scheme_success_chance
				}
				modifier = {
					factor = 0.01
				}
				trigger_event = anb_enhance_ability_outcome.4
				custom_tooltip = anb_enhance_ability_outcome.1.a_nothing_tt
			}
		}

		stress_impact = {
			base = major_stress_impact_gain
			magical_affinity_1 = minor_stress_impact_loss
			magical_affinity_2 = medium_stress_impact_loss
			magical_affinity_3 = major_stress_impact_loss
		}

		if = {
			limit = {
				scope:scheme = {
					has_variable = risky_spellbook_taken
				}
			}
			custom_tooltip = anb_enhance_ability_outcome.1.a_risky_spellbook_tt
			stress_impact = {
				base = minor_stress_impact_gain	
			}
		}

		# #Success roll
		# random = {
		# 	chance = scope:scheme.scheme_success_chance
		# 	save_scope_value_as = {
		# 		name = spell_successful
		# 		value = yes
		# 	}
		# }
		# #Failure roll
		# random = {
		# 	chance = 100
		# 	modifier = {
		# 		subtract = scope:scheme.scheme_success_chance

		# 	}
		# 	save_scope_value_as = {
		# 		name = spell_failure
		# 		value = yes
		# 	}
		# }
		# if = {
		# 	limit = { exists = scope:spell_successful }
		# 	trigger_event = anb_enhance_ability_outcome.2
		# }
		# else = {
		# 	#Spell fails (bad result)
		# 	if = {
		# 		limit = { exists = scope:spell_failure }
		# 		trigger_event = anb_enhance_ability_outcome.3
		# 	}
		# 	#Spell had no effect
		# 	else = {
		# 		trigger_event = anb_enhance_ability_outcome.4
		# 	}
		# }
	}

	#I need more preparation time!
	option = {
		name = anb_enhance_ability_outcome.1.b
		scope:scheme = {
			add_scheme_progress = decline_execution_setback
		}
		stress_impact = {
			impatient = minor_stress_impact_gain
		}
	}

	after = {
		#Great success roll
		#Just rolled as scheme success chance divided by 10 - so if you succeed at 100% you have 10% chance for great success, if succeed at 50% you have 5% for great success, ect.
		random = {
			chance = scope:scheme.scheme_success_chance
			modifier = {
				factor = 0.1
			}
			save_scope_value_as = {
				name = spell_great_success
				value = yes
			}
		}

		if = { 
			limit = {
				exists = scope:spell_great_success
				exists = scope:spell_successful
			} 
			trigger_event = anb_enhance_ability_outcome.5
		}
		else_if = { limit = { exists = scope:spell_successful } 
			trigger_event = anb_enhance_ability_outcome.2
		}
		remove_variable = failure_chance
	}
}

#Spell Success!
anb_enhance_ability_outcome.2 = {
	type = character_event
	title = anb_enhance_ability_outcome.2.t
	desc = anb_enhance_ability_outcome.2.d

	theme = friendly
	left_portrait = {
		character = scope:target
		animation = happiness
	}

	option = {
		name = anb_enhance_ability_outcome.2.a
		scope:target = {
			if = {
				limit = { has_character_flag = enhance_ability_diplomacy }
				add_character_modifier = {
					modifier = enhance_ability_diplomacy_success
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_martial }
				add_character_modifier = {
					modifier = enhance_ability_martial_success
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_stewardship }
				add_character_modifier = {
					modifier = enhance_ability_stewardship_success
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_intrigue }
				add_character_modifier = {
					modifier = enhance_ability_intrigue_success
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_learning }
				add_character_modifier = {
					modifier = enhance_ability_learning_success
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_prowess }
				add_character_modifier = {
					modifier = enhance_ability_prowess_success
					years = 5
				}
			}
			add_character_flag = {
				flag = recently_had_enhance_ability_attempt
				years = 5
			}
		}

		scope:scheme = {
			end_scheme = yes
		}
	}
}

#Spell Failure...
anb_enhance_ability_outcome.3 = {
	type = character_event
	title = anb_enhance_ability_outcome.3.t
	desc = anb_enhance_ability_outcome.3.d

	theme = friendly
	left_portrait = {
		character = scope:owner
		animation = worry
	}
	right_portrait = {
		trigger = { NOT = { scope:owner = scope:target } }
		character = scope:target
		animation = anger
	}

	option = {
		name = anb_enhance_ability_outcome.3.a
		scope:target = {
			if = {
				limit = { has_character_flag = enhance_ability_diplomacy }
				add_character_modifier = {
					modifier = enhance_ability_diplomacy_fail
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_martial }
				add_character_modifier = {
					modifier = enhance_ability_martial_fail
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_stewardship }
				add_character_modifier = {
					modifier = enhance_ability_stewardship_fail
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_intrigue }
				add_character_modifier = {
					modifier = enhance_ability_intrigue_fail
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_learning }
				add_character_modifier = {
					modifier = enhance_ability_learning_fail
					years = 5
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_prowess }
				add_character_modifier = {
					modifier = enhance_ability_prowess_fail
					years = 5
				}
			}
			add_character_flag = {
				flag = recently_had_enhance_ability_attempt
				years = 5
			}	
		}
		
		scope:scheme = {
			end_scheme = yes
		}
		reverse_add_opinion = {
			target = scope:recipient
			modifier = angry_opinion
			opinion = -30
		}
	}
}

#Nothing happened
anb_enhance_ability_outcome.4 = {
	type = character_event
	title = anb_enhance_ability_outcome.4.t
	desc = anb_enhance_ability_outcome.4.d

	theme = friendly
	left_portrait = {
		character = scope:owner
		animation = disbelief
	}
	right_portrait = {
		trigger = { NOT = { scope:owner = scope:target } }
		character = scope:target
		animation = idle
	}

	option = {
		name = anb_enhance_ability_outcome.4.a
		scope:target = {
			add_character_flag = {
				flag = recently_had_enhance_ability_attempt
				years = 5
			}
		}	
		scope:scheme = {
			end_scheme = yes
		}
	}
}

#Strong success
anb_enhance_ability_outcome.5 = {
	type = character_event
	title = anb_enhance_ability_outcome.5.t
	desc = anb_enhance_ability_outcome.5.d

	theme = friendly
	left_portrait = {
		character = scope:target
		animation = shock
	}

	option = {
		name = anb_enhance_ability_outcome.5.a
		scope:target = {
			if = {
				limit = { has_character_flag = enhance_ability_diplomacy }
				add_character_modifier = {
					modifier = enhance_ability_diplomacy_success
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_martial }
				add_character_modifier = {
					modifier = enhance_ability_martial_success
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_stewardship }
				add_character_modifier = {
					modifier = enhance_ability_stewardship_success
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_intrigue }
				add_character_modifier = {
					modifier = enhance_ability_intrigue_success
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_learning }
				add_character_modifier = {
					modifier = enhance_ability_learning_success
				}
			}
			else_if = {
				limit = { has_character_flag = enhance_ability_prowess }
				add_character_modifier = {
					modifier = enhance_ability_prowess_success
				}
			}
			add_character_flag = {
				flag = recently_had_enhance_ability_attempt
				years = 5
			}
		}
		scope:scheme = {
			end_scheme = yes
		}
	}
}