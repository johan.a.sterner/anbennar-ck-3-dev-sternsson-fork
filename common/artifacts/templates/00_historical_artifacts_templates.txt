﻿# Anbennar: removed vanilla artifacts

# ark_covenant_template = { } # Anbennar

# banner_thankfulness = { } # Anbennar

# babr_template = { } # Anbennar

# justinian_template = { } # Anbennar

# papal_tiara_template = { } # Anbennar

# al_taj_template = { } # Anbennar

# cup_jamshid_template = { } # Anbennar

# david_harp_template = { } # Anbennar

# crown_iron_template = { } # Anbennar

# reichskrone_template = { } # Anbennar

# throne_charlemagne_template = { } # Anbennar

# throne_solomon_template = { } # Anbennar

# throne_scone_template = { } # Anbennar

# kaviani_template = { } # Anbennar

# edessa_template = { } # Anbennar

# tizona_template = { } # Anbennar

# muhammad_sword_template = { } # Anbennar

# christian_artifact_template = { } # Anbennar

# islam_artifact_template = { } # Anbennar

# buddhism_artifact_template = { } # Anbennar

# attila_template = { } # Anbennar

# pentapyrgion_template = { } # Anbennar

# branch_zoroastr_template = { } # Anbennar

# branch_germanic_template = { } # Anbennar

# branch_slavic_template = { } # Anbennar

# branch_boog_template = { } # Anbennar

# branch_hinduism_template = { } # Anbennar

# relic_finno_ugric_template = { } # Anbennar

# jainism_artifact_template = { } # Anbennar

branch_general_template = {
	can_equip = {
		always = yes
	}

	# can this character benefit from the full modifiers of the artifact?
	can_benefit = {
		scope:artifact = { exists = var:relic_religion }
		has_religion = scope:artifact.var:relic_religion
	}

	# if a given character does not pass the "can_benefit" trigger then this modifier will be applied instead.
	fallback = {
		court_grandeur_baseline_add = 3
	}

	unique = yes
}

### Court Artifacts Templates ###

# cross_template = { } # Anbennar

# christian_relic_template = { } # Anbennar

# islam_relic_template = { } # Anbennar

# buddhism_relic_template = { } # Anbennar

# judaism_relic_template = { } # Anbennar

religious_statue_template = {
	can_equip = {
		always = yes
	}

	can_benefit = {
		scope:artifact = { has_variable = statue_religion }
		religion = scope:artifact.var:statue_religion
	}

	fallback = {
		court_grandeur_baseline_add = 3
	}
}

# 6050_relic_template = { } # Anbennar

# saintly_bones_template = { } # Anbennar

general_unique_template = {
	can_equip = {
		always = yes
	}

	can_benefit = {
	}

	fallback = {

	}

	unique = yes
}

child_toy_template = {
	can_equip = {
		trigger_if = {
			limit = {
				NOT = {
					scope:artifact = { category = court }
				}
			}
			custom_tooltip = {
				text = child_toy_template_tt
				age < 16
			}
		}
	}

	can_benefit = {
	}

	fallback = {

	}
}

adults_only_template = {
	can_equip = {
		age > 18
	}

	can_benefit = {
	}

	fallback = {

	}
}

### FP2 Templates ###

# fp2_chalice_dona_urraca_template = { } # Anbennar

# fp2_santiago_aquamanile_template = { } # Anbennar

# fp2_santiago_bells_template = { } # Anbennar

# crown_christian_relic_template = { } # Anbennar
