﻿cardesti = {
	color = { 182 131 46 }

	ethos = ethos_communal
	heritage = heritage_cardesti
	language = language_cardesti
	martial_custom = martial_custom_male_only
	traditions = {
		tradition_diasporic
		tradition_festivities
		tradition_caravaneers
	}
	dlc_tradition = {
		trait = tradition_music_theory
		requires_dlc_flag = royal_court
	}
	
	name_list = name_list_cardesti
	
	coa_gfx = { western_coa_gfx }
	building_gfx = { western_building_gfx }
	clothing_gfx = { western_clothing_gfx }
	unit_gfx = { western_unit_gfx }
	
	ethnicities = {
		8 = indian
		2 = mediterranean_byzantine
	}
}