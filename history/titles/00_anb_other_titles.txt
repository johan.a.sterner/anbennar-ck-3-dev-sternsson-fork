﻿
d_minar_temple = {
	1000.1.1 = {
		liege = k_lorent
		holder = 170
	}
}

d_highest_moon = {
	1000.1.1 = {
		liege = k_dameria
		holder = damerian_0001 # High Priestess Aria of the Highest Moon
	}
}

d_highcour = {
	1000.1.1 = {
		liege = k_lorent
		holder = lorentish_0001
	}
}

d_necropolis = {
	1008.4.9 = {
		holder = necropolis_0001 # High Priest Folcard
	}
}

e_castanor = {
	-640.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_I # Castan I the Progenitor
	}
	-621.1.1 = { # right year, random date PLACEHOLDER
		holder = 0 # Castanorian Interregnum
	}
	-613.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_II # Castan II Beastbane
	}
	-569.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_III # Castan III Dwarf-friend
	}
	-521.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_IV # Castan IV Realmbuilder
	}
	-471.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_V # Castan V the Great
	}
	-420.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VI # Castan VI Giantsbane
	}
	-357.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VII # Castan VII the Preserver
	}
	-307.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_VIII # Castan VIII
	}
	-302.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_IX # Castan IX Peacemaker
	}
	-294.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_X # Castan X the Quareller
	}
	-248.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XI # Castan XI the Righteous
	}
	-244.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XII # Castan XII the Old
	}
	-144.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIII # Castan XIII
	}
	-132.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIV # Castan XIV
	}
	-111.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XV # Castan XV the Short
	}
	-111.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVI # Castan XVI
	}
	-83.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVII # Castan XVII
	}
	-70.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XVIII # Castan XVIII
	}
	-59.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XIX # Castan XIX
	}
	-33.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XX # Castan XX the Youthful
	}
	-30.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXI # Castan XXI the Compassionate
	}
	-27.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXII # Castan XXII the Eccentric
	}
	-26.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIII # Castan XXIII
	}
	-13.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIV # Castan XXIV the Unready
	}
	4.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXV # Castan XXV the Perished
	}
	5.1.1 = { # right year, random date PLACEHOLDER
		holder = 0 # no one 
	}
	31.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVI # Castan XXVI the Unwavering
	}
	43.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVII # Castan XXVII the Sickly
	}
	101.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXVIII # Castan XXVIII
	}
	118.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXIX # Castan XXIX
	}
	157.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXX # Castan XXX
	}
	201.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXI # Castan XXXI the Venerable
	}
	307.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXII # Castan XXXII
	}
	323.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIII # Castan XXXIII
	}
	374.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIV # Castan XXXIV
	}
	398.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXV # Castan XXXV the Defiant
	}
	474.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVI # Castan XXXVI the Dragonfriend
	}
	500.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVII # Castan XXXVII the Rebuilder
	}
	518.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXVIII # Castan XXXVIII the Humble
	}
	566.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XXXIX # Castan XXXIX the Silent
	}
	666.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XL # Castan XL the Steward
	}
	669.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLI # Castan XLI the Vengeful
	}
	700.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLII # Castan XLII the Cowardly
	}
	707.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIII # Castan XLIII
	}
	728.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIV # Castan XLIV
	}
	761.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLV # Castan XLV
	}
	793.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVI # Castan XLVI
	}
	809.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVII # Castan XLVII Frostbane
	}
	859.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLVIII # Castan XLVIII
	}
	865.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_XLIX # Castan XLIX Ebonfrost
	}
	893.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_L # Castan L
	}
	895.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_LI # Castan LI
	}
	936.1.1 = { # right year, random date PLACEHOLDER
		holder = castan_LII # Castan LII the Kind
	}
	978.1.5 = {
		holder = castan_LIII # Castan LIII the Enthralled
	}
	1015.12.14 = {
		holder = the_sorcerer_king # Nichmer the Sorcerer-King
	}
	1020.10.31 = {	#Battle of Trialmount
		holder = 0
	}
}

e_lencenor = {
	525.12.25 = { # Formation of the High Kingdom
		holder = lorenan_the_great # Lorenan the Great
	}
	589.1.28 = {
		holder = rubentis_0001 # Ruben I Rubentis
	}
	595.2.18 = {
		holder = rubentis_0004 # Ruben II Rubentis
	}
	600.3.16 = {
		holder = rubentis_0005 # Emmeran I Rubentis
	}
	622.10.3 = { # War of the Three Roses
		holder = 0
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	885.1.1 = {
		holder = 0
	}
}

k_redfort = {
	525.12.25 = { # Formation of the High Kingdom
		holder = lorenan_the_great # Lorenan the Great
	}
	589.1.28 = {
		holder = rubentis_0001 # Ruben I Rubentis
	}
	595.2.18 = {
		holder = rubentis_0004 # Ruben II Rubentis
	}
	600.3.16 = {
		holder = rubentis_0005 # Emmeran I Rubentis
	}
	622.10.3 = { # War of the Three Roses
		holder = 0
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		holder = derhilde_reaverqueen # Derhilde Reaverqueen
	}
	885.1.1 = {
		holder = rubentis_0022 # Lorenan III Rubentis
	}
	897.8.30 = {
		holder = lorentis_0001 # Lorevarn II Lorentis
	}
	900.1.1 = { # Foundation of the Kingdom of Lorent
		holder = 0
	}
}

k_redwood = {
	543.1.1 = {
		liege = e_lencenor
		holder = rewantis_0001 # Rewan I "Ironbark" Rewantis
	}
	582.12.2 = {
		holder = rewantis_0004 # Maren I Rewantis
	}
	623.9.28 = {
		holder = rewantis_0005 # Rewan II Rewantis
	}
	622.10.3 = { # War of the Three Roses
		liege = 0
	}
	641.3.31 = {
		holder = rewantis_0006 # Loren I Rewantis
	}
	665.2.10 = {
		holder = rewantis_0007 # Maren II Rewantis
	}
	670.9.18 = {
		holder = rewantis_0008 # Rewan III Rewantis
	}
	695.1.3 = {
		holder = rewantis_0009 # Maren III "the Poet" Rewantis
	}
	724.10.10 = {
		holder = rewantis_0010 # Rewan IV "the Grey" Rewantis
	}
	770.9.8 = {
		holder = rewantis_0012 # Ruben I Rewantis
	}
	801.11.2 = {
		holder = rewantis_0014 # Rewan V "the White" Rewantis
	}
	814.10.5 = {
		holder = rewantis_0015 # Sorewan I Rewantis
	}
	844.10.16 = {
		holder = rewantis_0017 # Rewan VI Rewantis
	}
	860.4.9 = {
		holder = rewantis_0018 # Lorevarn I Rewantis
	}
	879.5.10 = {
		holder = lorentis_0001 # Lorevarn II Lorentis
	}
	900.1.1 = { # Foundation of the Kingdom of Lorent
		holder = 0
	}
}

e_lord_regent_of_lencenor = {
	622.10.3 = {
		holder = lorenti_0001 # Hesdren of Threeflowers
	}
	640.11.9 = {
		holder = harascilde_0008 # Caradan I of Harascilde
	}
	668.11.13 = {
		holder = aldegarde_0004 # Lorens Aldegarde
	}
	676.2.18 = {
		holder = crownscour_0003 # Florien of Crownscour
	}
	697.2.2 = {
		holder = harascilde_0010 # Caradan II of Harascilde
	}
	712.9.12 = {
		holder = lorenti_0002 # Rugern, High Priest of Highcour
	}
	730.8.24 = {
		holder = harascilde_0013 # Artan I "the Falcon" of Harascilde
	}
	754.5.14 = {
		holder = harascilde_0014 # Lorens II of Harascilde
	}
	789.10.12 = {
		holder = harascilde_0015 # Ruben "the Handsome" of Harascilde
	}
	810.6.8 = {
		holder = harascilde_0017 # Artan II of Harascilde
	}
	855.12.13 = {
		holder = harascilde_0018 # Caradan III "the Brave" of Harascilde
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		holder = 0
	}
}

k_lord_regent_of_redfort = {
	622.10.3 = {
		holder = lorenti_0001 # Hesdren of Threeflowers
	}
	640.11.9 = {
		holder = harascilde_0008 # Caradan I of Harascilde
	}
	668.11.13 = {
		holder = aldegarde_0004 # Lorens Aldegarde
	}
	676.2.18 = {
		holder = crownscour_0003 # Florien of Crownscour
	}
	697.2.2 = {
		holder = harascilde_0010 # Caradan II of Harascilde
	}
	712.9.12 = {
		holder = lorenti_0002 # Rugern, High Priest of Highcour
	}
	730.8.24 = {
		holder = harascilde_0013 # Artan I "the Falcon" of Harascilde
	}
	754.5.14 = {
		holder = harascilde_0014 # Lorens II of Harascilde
	}
	789.10.12 = {
		holder = harascilde_0015 # Ruben "the Handsome" of Harascilde
	}
	810.6.8 = {
		holder = harascilde_0017 # Artan II of Harascilde
	}
	855.12.13 = {
		holder = harascilde_0018 # Caradan III "the Brave" of Harascilde
	}
	869.2.26 = { # Derannic Conquest of Lencenor
		holder = 0
	}
}

d_knights_of_the_saltmarch = {
	1020.1.1 = {
		liege = "k_verne"
	}
	1020.1.1 = {
		holder = 179
	}
}

d_skaldskola = {
	1003.1.1 = {
		holder = 300012
	}
}
