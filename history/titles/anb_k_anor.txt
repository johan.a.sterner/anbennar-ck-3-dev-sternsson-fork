﻿
k_anor = {
	1000.1.1 = { change_development_level = 8 }
	1020.10.1 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 90 # Lain sil Anor
	}
}

d_sapphirecrown = {
	1021.10.31 = {		#Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 90 # Anor King
	}
}

d_southgate = {
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_anor
		holder = 574 # Lucan Silurion, one of the Silurion quintuplets
	}
}

c_southgate = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 574 # Lucan Silurion, one of the Silurion quintuplets
	}
}

c_ar_urion = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 574 # Lucan Silurion, one of the Silurion quintuplets
	}
}

c_the_manorfields = {
	1000.1.1 = { change_development_level = 11 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 574 # Lucan Silurion, one of the Silurion quintuplets
	}
}

d_foarhal = {
	1000.1.1 = { change_development_level = 11 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		liege = k_anor
		holder = 514 # Iacob the Betrayer
	}
}

c_foarhal = {
	1000.1.1 = { change_development_level = 12 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 514 # Iacob the Betrayer
	}
}

c_bradnath = {
	1000.1.1 = { change_development_level = 12 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 514 # Iacob the Betrayer
	}
}

c_mintirley = {
	1000.1.1 = { change_development_level = 8 }
	1021.10.31 = {	# Grand Ball of Anbenncost + Treaty of Anbenncost
		holder = 514 # Iacob the Betrayer
	}
}

d_bradescker = {
	1000.1.1 = { change_development_level = 10 }
}

c_marronath = {
	1000.1.1 = { change_development_level = 10 }
	1021.10.31 = {
		liege = d_foarhal
		holder = escanni_0011
	}
}

d_ardent_glade = {
	1021.10.31 = {
		liege = k_anor
		holder = escanni_0001
	}
}

c_ardent_keep = {
	1021.10.31 = {
		holder = escanni_0001
	}
}

c_cadells_rest = {
	1021.10.31 = {
		holder = escanni_0001
	}
}

c_grenmore = {
	1021.10.31 = {
		holder = escanni_0001
	}
}

c_eswall = {
	1021.10.31 = {
		liege = d_ardent_glade
		holder = escanni_0004
	}
}

c_oldhaven = {
	1021.10.31 = {
		liege = k_anor
		holder = escanni_0007
	}
}

c_annistoft = {
	1021.10.31 = {
		liege = k_anor
		holder = escanni_0005
	}
}

c_hagstow = {
	1021.10.31 = {
		liege = k_anor
		holder = escanni_0007
	}
}

